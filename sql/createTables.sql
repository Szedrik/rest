USE adverts_db;

CREATE TABLE Campaigns
(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(40),
    startDate DATETIME,
    endDate DATETIME,
    clickPrice REAL
);

CREATE TABLE RecommendationTypes
(
    id int primary key,
    name varchar(10)
);

INSERT INTO RecommendationTypes VALUES (1, 'advert');
INSERT INTO RecommendationTypes VALUES (2, 'content');

CREATE TABLE Recommendations
(
    id int primary key AUTO_INCREMENT,
    typeId int references RecommendationType(id),
    campaignId int,
    title VARCHAR(60),
    text VARCHAR(100),
    imageLink VARCHAR(250),
    foreign key (campaignId) references Campaigns (id),
    constraint Recommendation_AltPk unique
        (id, typeId)
);

CREATE TABLE Adverts
(
    id int primary key,
    typeId int DEFAULT 1,
    advertiser char (60),
    foreign key (id, typeId) references Recommendations(id, typeId)
);

CREATE TABLE Contents
(
    id int primary key,
    typeId int DEFAULT 2,
    foreign key (id, typeId)
            references Recommendations(id, typeId)
);


#Defaultkampagne
INSERT INTO Campaigns
(
    name, startDate, endDate, clickPrice
)
VALUES
(
    'testcampaign', '01.01.2000', '01.01.2001', 0.5
);

#erstelle eine ad
INSERT INTO Recommendations
(
    id, typeId, campaignId, title, text, imageLink
)
VALUES
(
    1, 1, 2, 'adTitle', 'adText', 'adLink'
);

INSERT INTO Adverts
(
    id, advertiser
)
Values
(
    1, 'specificAdvertiser'
);

#erstelle eine contentRecommendation
INSERT INTO Recommendations
(
    id, typeId, campaignId, title, text, imageLink
)
VALUES
(
    2, 2, 1, 'contentTitle', 'contentText', 'contentLink'
);

INSERT INTO Contents
(
    id
)
Values
(
    2
);

DELETE Recommendations, Adverts  FROM Adverts JOIN Recommendations ON Adverts.id = Recommendations.id WHERE Recommendations.campaignId = '1';











