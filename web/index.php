<?php

require_once __DIR__.'/../vendor/autoload.php';

use Controller\CampaignController;
use Controller\ContentController;
use Controller\AdvertController;

$app = new Silex\Application();

try {

    //open the database
    $app["db"] = function()
    {
        // Please provide you database user and its password in the empty parameter strings
        $db = new PDO('mysql:host=localhost;dbname=adverts_db', '', '');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    };

    //Validator
    $app->register(new Silex\Provider\ValidatorServiceProvider());

    // Controller
    $app->mount('/contents', new ContentController());
    $app->mount('/campaigns', new CampaignController());
    $app->mount('/adverts', new AdvertController());

    $app->run();

}
catch(PDOException $e)
{

    print 'Exception : '.$e->getMessage();
}

?>
