<?php
namespace Exceptions;

use Exceptions\CustomException;
use Silex\Tests\Provider\ValidatorServiceProviderTest\Constraint\Custom;

class ExceptionFactory
{

    public static function getEntityNotFoundException()
    {
        return new CustomException('the requestet Entity was not Found', 404);
    }

    public static function getEntityNotValidException($errors)
    {
        $errorMsg = 'The entity could not be created :';
        foreach ($errors as $key => $val)
        {
            $errorMsg .= $key.'('.$val.');';
        }

        return new CustomException($errorMsg, 412);

    }
}