<?php
    namespace Model;

    use Symfony\Component\Validator\Constraints as Assert;
    use Silex\Application;

    class Advert extends Recommendation  implements \JsonSerializable
    {
        const typeId = 1;

        private $advertiser;

        public function getAdvertiser()
        {
            return $this->advertiser;
        }

        public function setAdvertiser($advertiser)
        {
            $this->advertiser = $advertiser;
        }

        function __construct(array $data)
        {
            foreach($data as $key => $val) {
                if(property_exists(__CLASS__,$key)) {
                    $this->$key = $val;
                }
            }
        }

        public function jsonSerialize()
        {
            return get_object_vars($this);
        }

        public function validate(Application $app)
        {

            $data = array('id' => $this->getId(), 'campaignId' => $this->getCampaignId(), 'title' => $this->getTitle(), 'text' => $this->getText(),
                'imageLink' => $this->getImageLink(), 'advertiser' => $this->getAdvertiser());


            $constraint = new Assert\Collection( array(
                'id' => null,
                'campaignId' => null,
                'title' => array(new Assert\NotNull(), new Assert\Length(array('max' => 60))),
                'text' => array(new Assert\NotNull(), new Assert\Length(array('max' => 100))),
                'imageLink' => array(new Assert\NotNull(),new Assert\NotBlank()),
                'advertiser' => new Assert\NotNull()
            ));

            $errors = $app['validator']->validate($data, $constraint);
            foreach ($errors as $property => $error) {
                $responseArray[$error->getPropertyPath()] =  $error->getMessage();
            }
            return $responseArray;
        }

    }

?>