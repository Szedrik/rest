<?php
    namespace Model;

    abstract class Recommendation
    {
        protected $id;
        protected $campaignId;
        protected $title;
        protected $text;
        protected $imageLink;

        public function setTitle($title)
        {
            $this->title = $title;
        }

        public function getTitle()
        {
            return $this->title;
        }

        public function setText($text)
        {
            $this->text = $text;
        }

        public function getText()
        {
            return $this->text;
        }

        public function setImageLink($imageLink)
        {
            $this->imageLink = $imageLink;
        }

        public function getImageLink()
        {
            return $this->imageLink;
        }

        public function getId()
        {
            return $this->id;
        }

        public function setId($id)
        {
            $this->id = $id;
        }

        public function getCampaignId()
        {
            return $this->campaignId;
        }

        public function setCampaignId($campaignId)
        {
            $this->campaignId = $campaignId;
        }




    }

?>