<?php
    namespace Model;

    use Silex\Application;
    use Symfony\Component\Validator\Constraints as Assert;

    class Campaign implements \JsonSerializable
    {

        private $id;
        private $name;
        private $startDate;
        private $endDate;
        private $clickPrice;
        private $recommendations;

        /**
         * @return mixed
         */
        public function getRecommendations()
        {
            return $this->recommendations;
        }

        /**
         * @param mixed $recommendations
         */
        public function setRecommendations($recommendations)
        {
            $this->recommendations = $recommendations;
        }



        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @param mixed $id
         */
        public function setId($id)
        {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * @param mixed $name
         */
        public function setName($name)
        {
            $this->name = $name;
        }

        /**
         * @return mixed
         */
        public function getStartDate()
        {
            return $this->startDate;
        }

        /**
         * @param mixed $startDate
         */
        public function setStartDate($startDate)
        {
            $this->startDate = $startDate;
        }

        /**
         * @return mixed
         */
        public function getEndDate()
        {
            return $this->endDate;
        }

        /**
         * @param mixed $endDate
         */
        public function setEndDate($endDate)
        {
            $this->endDate = $endDate;
        }

        /**
         * @return mixed
         */
        public function getClickPrice()
        {
            return $this->clickPrice;
        }

        /**
         * @param mixed $clickPrice
         */
        public function setClickPrice($clickPrice)
        {
            $this->clickPrice = $clickPrice;
        }

        function __construct(array $data)
        {
            foreach($data as $key => $val) {
                if(property_exists(__CLASS__,$key)) {
                    $this->$key = $val;
                }
            }
        }

        public function jsonSerialize()
        {
            return get_object_vars($this);
        }

        public function validate(Application $app)
        {

            $data = array('id' => $this->getId(), 'name' => $this->getName(), 'startDate' => $this->getStartDate(), 'endDate' => $this->getEndDate(),
                'clickPrice' => $this->getClickPrice());


            $constraint = new Assert\Collection( array(
                'id' => null,
                'name' => array(new Assert\NotNull(), new Assert\Length(array('max' => 40))),
                'startDate' => array(new Assert\NotNull(), new Assert\Date()),
                'endDate' => array(new Assert\NotNull(), new Assert\Date()),
                'clickPrice' => array(new Assert\NotNull(),new Assert\NotBlank())
            ));

            $errors = $app['validator']->validate($data, $constraint);
            foreach ($errors as $property => $error) {
                $responseArray[$error->getPropertyPath()] =  $error->getMessage();
            }
            return $responseArray;
        }
    }
?>