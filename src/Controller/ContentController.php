<?php
namespace Controller;

use Exceptions\CustomException;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Repository\ContentRepository;
use Model\Content;
use Repository\CampaignRepository;


class ContentController implements ControllerProviderInterface
{

    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];
        $factory->get('/', 'Controller\ContentController::getAll');
        $factory->get('/{id}','Controller\ContentController::getContent');
        $factory->put('/{id}', 'Controller\ContentController::updateContent');
        $factory->delete('/{id}', 'Controller\ContentController::delete');
        $factory->delete('/', 'Controller\ContentController::deleteAll');

        return $factory;
    }

    //curl -X GET http://www.advertrest.com/index.php/contents/
    public function getAll(Application $app)
    {
        $repo = new ContentRepository($app);

        $result = $repo->getAll();
        return new Response(json_encode($result, JSON_NUMERIC_CHECK),200);
    }

    // curl -X GET http://www.advertrest.com/index.php/contents/2
    public function getContent(Application $app, int $id)
    {
        $repo = new ContentRepository($app);
        try
        {
            $result = $repo->get($id);
        }
        catch (CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response(json_encode($result, JSON_NUMERIC_CHECK),200);
    }

    // curl -X POST http://www.advertrest.com/index.php/campaigns/5/contents/ -d '{"title":"test3Title","text":"test33text", "imageLink":"link3"}' -H 'Content-Type: application/json'
    public function addContent(Application $app, $id)
    {
        /*
        Symfony\Component\HttpFoundation\Request[request]hatte immer einen leeren ParameterBag bei Content-Type; application/json ;
        Daher der Workaround über php;//input
        */
        $json = file_get_contents("php://input");
        $request = json_decode($json, true);

        if ($request == false)
        {
            return new Response('json not valid', 403);
        }
        try
        {

            if (!$this->campaignRecommendationTypeIsCorrect($app, $id))
            {
                return new Response('Campaign already stores Adverts', 412);
            }

            $repo = new ContentRepository($app);
            $content = new Content($request);
            $content->setCampaignId($id);

            $repo->save($content);
        }
        catch (CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response(json_encode($content, JSON_NUMERIC_CHECK), 201);
    }


    // curl -X PUT http://www.advertrest.com/index.php/contents/12 -d '{"campaignId": 1,"title":"test3Title","text":"test33text", "imageLink":"link3"}' -H 'Content-Type: application/json'
    public function updateContent(Application $app, int $id)
    {
        $json = file_get_contents("php://input");
        $request = json_decode($json, true);
        if ($request == false)
        {
            return new Response('json not valid', 403);
        }

        if (!$this->campaignRecommendationTypeIsCorrect($app, $request['campaignId']))
        {
            return new Response('Campaign already stores contents', 412);
        }

        $repo = new ContentRepository($app);

        try
        {
            $content = $repo->get($id);
            $content->setTitle($request['title']);
            $content->setText($request['text']);
            $content->setCampaignId($request['campaignId']);
            $content->setImageLink($request['imageLink']);
            $repo->update($content);
        }
        catch (CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response(json_encode($content, JSON_NUMERIC_CHECK),200);
    }


    public function deleteAllFromCampaign(Application $app, int $id)
    {
        $repo = new ContentRepository($app);
        try
        {
            $repo->deleteAllFromCampaign($id);
        }
        catch(CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response('contents from Campaign deleted', 200);
    }

    public function delete(Application $app, int $id)
    {
        $repo = new ContentRepository($app);
        try
        {
            $repo->delete($id);
        }
        catch(CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response('content deleted', 200);
    }

    public function deleteAll(Application $app)
    {
        $repo = new ContentRepository($app);
        try
        {
            $repo->deleteAll();
        }
        catch(CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response('All contents deleted', 200);
    }

    private function campaignRecommendationTypeIsCorrect($app, $id)
    {
        $campaignRepo = new CampaignRepository($app);
        $recommendations = $campaignRepo->getRecommendations($id);

        if (count($recommendations) > 0) {
            if (get_class($recommendations[0]) != 'Model\Content') {
                return false;
            }

        }
        return true;
    }

}