<?php
namespace Controller;

use Exceptions\CustomException;
use Exceptions\DoesNotExistException;
use Repository\AdvertRepository;
use Repository\ContentRepository;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Model\Campaign;
use Repository\CampaignRepository;

class CampaignController implements ControllerProviderInterface
{

    public function connect(Application $app)
    {
        $factory=$app['controllers_factory'];
        $factory->get('/','Controller\CampaignController::getAll');
        $factory->get('/{id}','Controller\CampaignController::getCampaign');
        $factory->put('/{id}', 'Controller\CampaignController::updateCampaign');
        $factory->post('/', 'Controller\CampaignController::addCampaign');

        $factory->post('/{id}/adverts/', 'Controller\AdvertController::addAdvert');
        $factory->post('/{id}/contents/', 'Controller\ContentController::addContent');

        $factory->delete('/{id}', 'Controller\CampaignController::deleteCampaign');
        $factory->delete('/{id}/adverts/', 'Controller\AdvertController::deleteAllFromCampaign');
        $factory->delete('/{id}/contents/', 'Controller\ContentController::deleteAllFromCampaign');
        return $factory;
    }

    // curl -X DELETE http://www.advertrest.com/index.php/campaigns/2
    public function deleteCampaign(Application $app,int $id)
    {
        $repo = new CampaignRepository($app);
        $recommendations = $repo->getRecommendations($id);
        if (count($recommendations)>0)
        {
            if (get_class($recommendations[0]) == 'Model\Advert')
            {
                $advertRepo = new AdvertRepository($app);
                $advertRepo->deleteAllFromCampaign($id);
            }
            else
            {
                $contentRepo = new ContentRepository($app);
                $contentRepo->deleteAllFromCampaign($id);
            }
        }

        try
        {
            $repo->delete($id);
        }
        catch (CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }

        return new Response('Campaign deleted', 200);

    }

    //curl -X GET http://www.advertrest.com/index.php/campaigns/
    public function getAll(Application $app)
    {
        $repo = new CampaignRepository($app);
        $result = $repo->getAll();
        return new Response(json_encode($result, JSON_NUMERIC_CHECK),200);
    }

    // curl -X GET http://www.advertrest.com/index.php/campaigns/2
    public function getCampaign(Application $app, int $id)
    {
        $repo = new CampaignRepository($app);
        try
        {
            $result = $repo->get($id);
        }
        catch (CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response(json_encode($result, JSON_NUMERIC_CHECK),200);
    }

    // curl -X POST http://www.advertrest.com/index.php/campaigns/ -d '{"name":"testname","startDate":"2014-09-23","endDate":"2014-12-23", "clickPrice":0.5}' -H 'Content-Type: application/json'
    public function addCampaign(Application $app)
    {
        /*
        Symfony\Component\HttpFoundation\Request[request]hatte immer einen leeren ParameterBag bei Content-Type; application/json ;
        Daher der Workaround über php;//input
        */
        $json = file_get_contents("php://input");
        $request = json_decode($json, true);

        if ($request == false)
        {
            return new Response('json not valid', 403);
        }

        $campaignRepository = new CampaignRepository($app);
        $campaign = new Campaign($request);
        try
        {
            $campaignRepository->save($campaign);
        }
        catch (CustomException $e)
        {
            return new Response($e->getMessage(), $e->getCode());

        }
        return new Response(json_encode($campaign, JSON_NUMERIC_CHECK), 201);
    }

    // curl -X PUT http://www.advertrest.com/index.php/campaigns/50 -d '{"name":"testname","startDate":"01.01.3000","endDate":"01.01.3003", "clickPrice":0.5}' -H 'Content-Type: application/json'
    public function updateCampaign(Application $app, int $id)
    {
        $json = file_get_contents("php://input");
        $request = json_decode($json, true);
        if ($request == false)
        {
            return new Response('json not valid', 403);
        }

        try
        {
            $repo = new CampaignRepository($app);
            $campaign = $repo->get($id);
            $campaign->setName($request['name']);
            $campaign->setStartDate($request['startDate']);
            $campaign->setEndDate($request['endDate']);
            $campaign->setClickPrice($request['clickPrice']);
            $repo->update($campaign);
        }
        catch (CustomException $e)
        {
            return new Response($e->getMessage(),$e->getCode());
        }

        return new Response(json_encode($campaign, JSON_NUMERIC_CHECK),200);
    }

}