<?php
namespace Controller;

use Exceptions\CustomException;
use Repository\CampaignRepository;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\Validator\Constraints as Assert;
use Repository\AdvertRepository;
use Symfony\Component\HttpFoundation\Response;
use Model\Advert;


class AdvertController implements ControllerProviderInterface
{

    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];
        $factory->get('/', 'Controller\AdvertController::getAll');
        $factory->get('/{id}','Controller\AdvertController::getAdvert');
        $factory->put('/{id}', 'Controller\AdvertController::updateAdvert');
        $factory->delete('/{id}', 'Controller\AdvertController::delete');
        $factory->delete('/', 'Controller\AdvertController::deleteAll');

        return $factory;
    }

    //curl -X GET http://www.advertrest.com/index.php/adverts/
    public function getAll(Application $app)
    {
        $repo = new AdvertRepository($app);
        $result = $repo->getAll();
        return new Response(json_encode($result, JSON_NUMERIC_CHECK),200);
    }

    // curl -X GET http://www.advertrest.com/index.php/adverts/1
    public function getAdvert(Application $app, int $id)
    {
        $repo = new AdvertRepository($app);
        try
        {
            $result = $repo->get($id);
        }
        catch (CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response(json_encode($result, JSON_NUMERIC_CHECK),200);
    }

    // curl -X POST http://www.advertrest.com/index.php/campaigns/6/adverts/ -d '{"title":"test3Title","text":"test33text", "advertiser":"advertiser3", "imageLink":"link3"}' -H 'Content-Type: application/json'

    public function addAdvert(Application $app, $id)
    {
        /*
        Symfony\Component\HttpFoundation\Request[request]hatte immer einen leeren ParameterBag bei Content-Type; application/json ;
        Daher der Workaround über php;//input
        */
        $json = file_get_contents("php://input");
        $request = json_decode($json, true);

        if ($request == false) {
            return new Response('json not valid', 403);
        }
        try
        {
            if (!$this->campaignRecommendationTypeIsCorrect($app, $id))
            {
                return new Response('Campaign already stores contents', 412);
            }

            $repo = new AdvertRepository($app);
            $advert = new Advert($request);
            $advert->setCampaignId($id);

            $repo->save($advert);
        }
        catch (CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }

        return new Response(json_encode($advert, JSON_NUMERIC_CHECK), 201);
    }

    // curl -X PUT http://www.advertrest.com/index.php/adverts/14 -d '{"campaignId": 2,"title":"test3Title","text":"test33text", "advertiser":"advertiser3", "imageLink":"link3"}' -H 'Content-Type: application/json'
    public function updateAdvert(Application $app, int $id)
    {
        $json = file_get_contents("php://input");
        $request = json_decode($json, true);
        if ($request == false)
        {
            return new Response('json not valid', 403);
        }

        if (!$this->campaignRecommendationTypeIsCorrect($app, $request['campaignId']))
        {
            return new Response('Campaign already stores contents', 412);
        }

        $repo = new AdvertRepository($app);

        try
        {
            $advert = $repo->get($id);
            $advert->setTitle($request['title']);
            $advert->setText($request['text']);
            $advert->setCampaignId($request['campaignId']);
            $advert->setImageLink($request['imageLink']);
            $advert->setAdvertiser($request['advertiser']);

            $repo->update($advert);
        }
        catch (CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }

        return new Response(json_encode($advert, JSON_NUMERIC_CHECK),200);
    }

    public function deleteAllFromCampaign(Application $app, int $id)
    {
        $repo = new AdvertRepository($app);
        try
        {
            $repo->deleteAllFromCampaign($id);
        }
        catch(CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response('adverts from Campaign deleted', 200);
    }

    public function delete(Application $app, int $id)
    {
        $repo = new AdvertRepository($app);
        try
        {
            $repo->delete($id);
        }
        catch(CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response('advert deleted', 200);
    }

    public function deleteAll(Application $app)
    {
        $repo = new AdvertRepository($app);
        try
        {
            $repo->deleteAll();
        }
        catch(CustomException $e)
        {
            return new Response ($e->getMessage(), $e->getCode());
        }
        return new Response('All adverts deleted', 200);
    }

    private function campaignRecommendationTypeIsCorrect($app, $id)
    {
        $campaignRepo = new CampaignRepository($app);
        $recommendations = $campaignRepo->getRecommendations($id);

        if (count($recommendations) > 0) {
            if (get_class($recommendations[0]) != 'Model\Advert') {
                return false;
            }

        }
        return true;
    }


}