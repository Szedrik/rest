<?php
namespace Repository;
/**
 * Created by PhpStorm.
 * User: webdev
 * Date: 23.11.16
 * Time: 09:55
 */

use PDO;
use Model\Content;
use Exceptions\ExceptionFactory;

class ContentRepository
{

    private $app;

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
    }

    public function getAll() {

        $db = $this->app["db"];
        $statement = $db->prepare("SELECT Contents.id, campaignId, title, text, imageLink From Contents JOIN Recommendations on Recommendations.id = Contents.id;");
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);


        $contents = array();
        foreach ($result as $contentData) {
            array_push($contents, new Content($contentData));
        }
        return $contents;
    }

    public function getAllFromCampaign(int $id)
    {
        $db = $this->app["db"];
        $statement = $db->prepare("SELECT Contents.id, campaignId, title, text, imageLink From Contents JOIN Recommendations on Recommendations.id = Contents.id HAVING campaignId = :id;");
        $statement->execute(array('id' => $id));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        $contents = array();
        foreach ($result as $contentData) {
            array_push($contents, new Content($contentData));
        }

        return $contents;
    }

    public function get(int $id) {

        if (!$this->contentExists($id)) {
            throw ExceptionFactory::getEntityNotFoundException();
        }

        $db = $this->app["db"];
        $statement = $db->prepare("SELECT Contents.id, campaignId, title, text, imageLink From Contents JOIN Recommendations on Recommendations.id = Contents.id HAVING Contents.id = :id;");
        $statement->execute(array('id' => $id));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        return new Content($result[0]);
    }

    public function save(Content $content)
    {

        $validationResult = $content->validate($this->app);
        if (count($validationResult) > 0)
        {
            throw ExceptionFactory::getEntityNotValidException($validationResult);
        }

        $db = $this->app["db"];

        $abstractStatement = $db->prepare("INSERT INTO Recommendations (campaignId, typeId, title, text, imageLink) VALUES( :campaignId, :typeId, :title, :text, :imageLink)");
        $abstractStatement->execute(array('campaignId' => $content->getCampaignId(), 'typeId' => $content::typeId, 'title' => $content->getTitle(), 'text' => $content->getText(), 'imageLink' => $content->getImageLink()));
        $content->setId($db->lastInsertId());

        $contentStatement = $db->prepare("INSERT INTO Contents (id) VALUES (:id)");
        $contentStatement->execute(array('id' => $content->getId()));

        return true;
    }

    public function update(Content $content)
    {

        if (!$this->contentExists($content->getId()))
        {
            throw ExceptionFactory::getEntityNotFoundException();
        }

        $validationResult = $content->validate($this->app);
        if (count($validationResult) > 0)
        {
            throw ExceptionFactory::getEntityNotValidException($validationResult);
        }

        $db = $this->app["db"];
        $statement = $db->prepare("
            UPDATE AbstractAdverts
            Set
              title = :title,
              text = :text,
              imageLink = :imageLink,
              campaignId = :campaignId
            WHERE
              id = :id;
        ");
        $statement->execute(array('title' => $content->getTitle(), 'text' => $content->getText(), 'imageLink' => $content->getImageLink(), 'campaignId' => $content->getCampaignId(), 'id' => $content->getId()));
        return true;
    }

    private function contentExists(int $id) {
        $db = $this->app["db"];
        $statement = $db->prepare("SELECT EXISTS (SELECT * From Contents WHERE id = :id) as exist;");
        $statement->execute(array('id' => $id));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['exist'];
    }

    public function deleteAllFromCampaign($id)
    {
        $db = $this->app["db"];

        $statement = $db->prepare("DELETE Recommendations, Contents   FROM Contents JOIN Recommendations ON Contents.id = Recommendations.id WHERE Recommendations.campaignId = :id;");
        $result = $statement->execute(array('id' => $id));

        return $result;
    }


    public function delete($id)
    {
        $db = $this->app["db"];
        $statement = $db->prepare("DELETE Recommendations, Contents  FROM Contents JOIN Recommendations ON Contents.id = Recommendations.id WHERE Contents.id = :id;");
        $result = $statement->execute(array('id' => $id));

        return $result;
    }

    public function deleteAll()
    {
        $db = $this->app["db"];
        $statement = $db->prepare("DELETE Recommendations, Contents  FROM Contents JOIN Recommendations ON Contents.id = Recommendations.id;");
        $result = $statement->execute();

        return $result;
    }


}