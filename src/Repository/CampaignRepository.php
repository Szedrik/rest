<?php
namespace Repository;

use Exceptions\ExceptionFactory;
use Exceptions\RepositoryException;
use Model\Campaign;
use PDO;
use Exceptions\DoesNotExistException;

class CampaignRepository
{
    private $app;

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
    }

    public function delete($id)
    {
        if (!$this->campaignExists($id)) {
            throw ExceptionFactory::getEntityNotFoundException();
        }

        $db = $this->app["db"];
        $statement = $db->prepare("DELETE From Campaigns WHERE id = :id");
        $result = $statement->execute(array('id' => $id));
        return $result;
    }

    public function get($id) {

        if (!$this->campaignExists($id)) {
            throw ExceptionFactory::getEntityNotFoundException();
        }

        $db = $this->app["db"];
        $statement = $db->prepare("SELECT * From Campaigns WHERE id = :id");
        $statement->execute(array('id' => $id));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        $campaign = new Campaign($result[0]);

        $recommendations = $this->getRecommendations($campaign->getId());
        $campaign->setRecommendations($recommendations);

        return $campaign;
    }

    public function getAll() {

        echo 'getAll';
        $db = $this->app["db"];
        $statement = $db->prepare("SELECT * From Campaigns");
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        $campaigns = array();
        foreach ($result as $row) {
            $campaign = new Campaign($row);
            $campaign->setRecommendations($this->getRecommendations($campaign->getId()));
            array_push($campaigns, $campaign);
        }
        return $campaigns;
    }

    public function save(Campaign $campaign)
    {
        $validationResult = $campaign->validate($this->app);
        if (count($validationResult) > 0)
        {
            throw ExceptionFactory::getEntityNotValidException($validationResult);
        }

        $db = $this->app["db"];
        $statement = $db->prepare("INSERT INTO Campaigns (name, startDate, endDate, clickPrice) VALUES( :name, :startDate, :endDate, :clickPrice)");
        $statement->execute(array('name' => $campaign->getName(), 'startDate' => $campaign->getStartDate(), 'endDate' => $campaign->getEndDate(), 'clickPrice' => $campaign->getClickPrice()));
        $campaign->setId($db->lastInsertId());
        return true;
    }

    public function update(Campaign $campaign)
    {
        if (!$this->campaignExists($campaign->getId()))
        {
            throw ExceptionFactory::getEntityNotFoundException();
        }

        $validationResult = $campaign->validate($this->app);
        if (count($validationResult) > 0)
        {
            throw ExceptionFactory::getEntityNotValidException($validationResult);
        }

        $db = $this->app["db"];
        $statement = $db->prepare("
            UPDATE Campaigns
            Set
              name = :name,
              startDate = :startDate,
              endDate = :endDate,
              clickPrice = :clickPrice
            WHERE
              id = :id;
        ");
        $statement->execute(array('name' => $campaign->getName(), 'startDate' => $campaign->getStartDate(), 'endDate' => $campaign->getEndDate(), 'clickPrice' => $campaign->getClickPrice()['clickPrice'], 'id' => $campaign->getId()));
        return true;
    }

    private function campaignExists(int $id) {
        $db = $this->app["db"];
        $statement = $db->prepare("SELECT EXISTS (SELECT * From Campaigns WHERE id = :id) as exist;");
        $statement->execute(array('id' => $id));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['exist'];
    }

    public function getRecommendations(int $id) {

        if (!$this->campaignExists($id)) {
            throw ExceptionFactory::getEntityNotFoundException();
        }

        $advertRepo = new AdvertRepository($this->app);
        $adverts = $advertRepo->getAllFromCampaign($id);

        if (count($adverts) < 1)
        {
            $contentRepo = new ContentRepository($this->app);
            $contents = $contentRepo->getAllFromCampaign($id);
            $recommendations = $contents;
        } else
        {
            $recommendations = $adverts;
        }
        return $recommendations;
    }
}