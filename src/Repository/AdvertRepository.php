<?php
namespace Repository;

use PDO;
use Symfony\Component\Validator\Constraints as Assert;
use Model\Advert;
use Exceptions\ExceptionFactory;

class AdvertRepository
{

    private $app;

    function __construct(\Silex\Application $app)
    {
        $this->app = $app;
    }

    public function getAll() {

        $db = $this->app["db"];
        $statement = $db->prepare("SELECT Adverts.id, campaignId, title, text, imageLink, Adverts.advertiser From Adverts JOIN Recommendations on Recommendations.id = Adverts.id;");
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        $adverts = array();
        foreach ($result as $advertData) {
            array_push($adverts, new Advert($advertData));
        }
        return $adverts;
    }

    public function get(int $id) {

        if (!$this->advertExists($id))
        {
            throw ExceptionFactory::getEntityNotFoundException();
        }

        $db = $this->app["db"];
        $statement = $db->prepare("SELECT Recommendations.id, campaignId, title, text, imageLink, advertiser From Adverts JOIN Recommendations on Recommendations.id = Adverts.id HAVING Recommendations.id = :id;");
        $statement->execute(array('id' => $id));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        return new Advert($result[0]);
    }

    public function getAllFromCampaign(int $id)
    {
        $db = $this->app["db"];
        $statement = $db->prepare("SELECT Adverts.id, campaignId, title, text, imageLink, Adverts.advertiser From Adverts JOIN Recommendations on Recommendations.id = Adverts.id HAVING campaignId = :id;");
        $statement->execute(array('id' => $id));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        $adverts = array();
        foreach ($result as $advertData) {
            array_push($adverts, new Advert($advertData));
        }
        return $adverts;
    }

    public function save(Advert $advert)
    {
        $validationResult = $advert->validate($this->app);
        if (count($validationResult) > 0)
        {
            throw ExceptionFactory::getEntityNotValidException($validationResult);
        }

        $db = $this->app["db"];
        $abstractStatement = $db->prepare("INSERT INTO Recommendations (campaignId, typeId, title, text, imageLink) VALUES( :campaignId, :typeId, :title, :text, :imageLink)");
        $abstractStatement->execute(array('campaignId' => $advert->getCampaignId(), 'typeId' => $advert::typeId, 'title' => $advert->getTitle(), 'text' => $advert->getText(), 'imageLink' => $advert->getImageLink()));
        $advert->setId($db->lastInsertId());

        $contentStatement = $db->prepare("INSERT INTO Adverts (id, advertiser) VALUES (:id, :advertiser)");
        $contentStatement->execute(array('id' => $advert->getId(), 'advertiser' => $advert->getAdvertiser()));

        return true;
    }

    public function update(Advert $advert)
    {

        if (!$this->advertExists($advert->getId()))
        {
            throw ExceptionFactory::getEntityNotFoundException();
        }

        $validationResult = $advert->validate($this->app);
        if (count($validationResult) > 0)
        {
            throw ExceptionFactory::getEntityNotValidException($validationResult);
        }

        $db = $this->app["db"];
        $abstractStatement = $db->prepare("
            UPDATE Recommendations
            Set
              title = :title,
              text = :text,
              imageLink = :imageLink,
              campaignId = :campaignId
            WHERE
              id = :id;
        ");
        $abstractStatement->execute(array('title' => $advert->getTitle(), 'text' => $advert->getText(), 'imageLink' => $advert->getImageLink(), 'campaignId' => $advert->getCampaignId(), 'id' => $advert->getId()));


        $statement = $db->prepare("
            UPDATE Adverts
            Set
              advertiser = :advertiser
            WHERE
              id = :id;
        ");
        $statement->execute(array('advertiser' => $advert->getAdvertiser(), 'id' => $advert->getId()));
        return true;
    }

    private function advertExists(int $id) {
        $db = $this->app["db"];
        $statement = $db->prepare("SELECT EXISTS (SELECT * From Adverts WHERE id = :id) as exist;");
        $statement->execute(array('id' => $id));
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['exist'];
    }

    public function deleteAllFromCampaign($id)
    {
        $db = $this->app["db"];
        $statement = $db->prepare("DELETE Recommendations, Adverts  FROM Adverts JOIN Recommendations ON Adverts.id = Recommendations.id WHERE Recommendations.campaignId = :id;");
        $result = $statement->execute(array('id' => $id));

        return $result;
    }

    public function delete($id)
    {
        $db = $this->app["db"];
        $statement = $db->prepare("DELETE Recommendations, Adverts  FROM Adverts JOIN Recommendations ON Adverts.id = Recommendations.id WHERE Adverts.id = :id;");
        $result = $statement->execute(array('id' => $id));

        return $result;
    }

    public function deleteAll()
    {
        $db = $this->app["db"];
        $statement = $db->prepare("DELETE Recommendations, Adverts  FROM Adverts JOIN Recommendations ON Adverts.id = Recommendations.id;");
        $result = $statement->execute();

        return $result;
    }

}
